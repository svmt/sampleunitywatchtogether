# Sceenic Unity SDK
Sceenic's Watch Together video-chat is a general-purpose video chatting infrastructure that is aimed to provide high-quality Audio and Video functionality that will allow you to create and provide engaging experiences in your application for your customers


## Documentation
Have a look at our official documentation site [documentation page](https://synchronization.docs.sceenic.co/)

## Requirements
- Unity 2020.3 or newer
- Watch Together SDK, API_KEY, and API_SECRET - can be retrieved in your private area once you log in - [Go to the private area](https://media.sceenic.co/)

In case you don't have access, but want to use our service please contact us through the website https://www.sceenic.co/

## Installation

To get our sample working, go to our [Unity tutorial](https://synchronization.docs.sceenic.co/tutorials/android-1/android) and follow the instructions