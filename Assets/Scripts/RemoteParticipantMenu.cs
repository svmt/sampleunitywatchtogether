using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteParticipantMenu : MonoBehaviour
{
    public GameObject kickButton;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!String.IsNullOrEmpty(SessionManager.instance.synchronizerId))
            kickButton.SetActive(true);
        else
        {
            kickButton.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
