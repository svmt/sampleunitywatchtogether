using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Mini "engine" for analyzing spectrum data
/// Feel free to get fancy in here for more accurate visualizations!
/// </summary>
public class AudioSpectrum : MonoBehaviour
{
    public AudioSource audio;
    private void Update()
    {
        // get the data
        if (audio == null)
        {
            AudioListener.GetSpectrumData(m_audioSpectrum, 0, FFTWindow.Hamming);
            if (m_audioSpectrum != null && m_audioSpectrum.Length > 0)
            {
                spectrumValue = m_audioSpectrum[0] * 100;
            }
        }
        else
        {
            audio.GetSpectrumData(m_audioSpectrumLocal, 0, FFTWindow.Hamming); 
            if (m_audioSpectrumLocal != null && m_audioSpectrumLocal.Length > 0)
            {
                spectrumValue = m_audioSpectrumLocal[0] * 100;
            }
        }
    
        // assign spectrum value
        // this "engine" focuses on the simplicity of other classes only..
        // ..needing to retrieve one value (spectrumValue)
      
    }

    private void Start()
    {
        /// initialize buffer
        m_audioSpectrum = new float[128];
        m_audioSpectrumLocal = new float[128];
    }

    // This value served to AudioSyncer for beat extraction
    public  float spectrumValue {get; private set;}

    // Unity fills this up for us
    private float[] m_audioSpectrum;
    private float[] m_audioSpectrumLocal;

}