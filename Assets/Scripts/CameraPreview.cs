using System;
using System.Collections;
using System.Collections.Generic;

using Unity.WebRTC;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CameraPreview : MonoBehaviour
{
    // Start is called before the first frame update

    public RawImage camRawImage;
    public WebCamTexture camTexture;
    public Text logText;
    public GameObject camPreviewObject;
    public int cameraID = 0;
    public UnityAction<int> changeCameraDevice;
    private WebCamDevice userCameraDevice;
    public UIManager UiManager;
    public Resolution[] camR;
    

    public IEnumerator StartRecording()
    {
        camPreviewObject.gameObject.SetActive(true);
        camRawImage.texture = null;
        logText.color = Color.red;
        UiManager.GetWebCamsList();
        userCameraDevice = WebCamTexture.devices[cameraID];
        
        camTexture = new WebCamTexture(userCameraDevice.name, SessionManager.minWidth, SessionManager.minHeight, 90);
       // camTexture = new WebCamTexture();
      
        camTexture.Play();
      
        yield return new WaitUntil(() => camTexture.didUpdateThisFrame);
        logText.text = "camera ready start playing";
        logText.color = Color.green;

        SessionManager.instance.userCameraTexture = camTexture;
        camRawImage.texture = camTexture;
    }

   public void ChangeDevice(int i)
   {
       camTexture.Stop();
       cameraID = i;
       StartCoroutine(StartRecording());
   }

   private void OnEnable()
   {
       changeCameraDevice += ChangeDevice;
   }

    private void OnDisable()
    {
        camPreviewObject.SetActive(false);
        logText.color = Color.red;
        camRawImage.texture = null;
        changeCameraDevice = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
