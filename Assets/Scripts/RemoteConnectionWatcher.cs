using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemoteConnectionWatcher : MonoBehaviour
{
    public GameObject connectionObject;
    public Image connectionImage;
    public Text connectionText;

    private RemoteParticipant remoteParticipant;
    // Start is called before the first frame update
    void Start()
    {
        remoteParticipant = GetComponent<RemoteParticipant>();
        SessionManager.instance.onRemoteConnectionLost += CheckParticipantConnection;
        SessionManager.instance.onParticipantReconnected += ShowReconnectedMessage;
        SessionManager.instance.onParticipantReconnecting += ShowReconnectingMessage;
    }

    void CheckParticipantConnection(string id)
    {
        if (id == remoteParticipant.id)
            ShowConnectionMessage(remoteParticipant.isConnectionLost);
    }

    void ShowConnectionMessage(bool isConnectedLost)
    {
        if (isConnectedLost)
        {
            connectionObject.SetActive(true);
            connectionText.text = "Connection Lost....";
            connectionImage.color = Color.red;
            
        }
        else
        {
            connectionText.text = "Connection restored....";
            connectionImage.color = Color.green;
            Invoke("HideMessage",5f);
        }
    }
    void ShowReconnectedMessage(string id, string oldParticipantId)
    {
       if (oldParticipantId == remoteParticipant.oldParticipantId)
       {
            connectionText.text = "Connection restored....";
            connectionImage.color = Color.green;
            Invoke("HideMessage",5f);
        }
    }
    void ShowReconnectingMessage(string id)
    {
        if (id == remoteParticipant.id)
        {
            connectionText.text = "Reconnecting....";
            connectionImage.color = Color.yellow;
         //   Invoke("HideMessage",5f);
        }
    }

    void HideMessage()
    {
        connectionObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
