using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ServerController.Scripts.Server;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using WCP;

public class UIManager : MonoBehaviour
{
    
    public Transform participantsHolder;
    public Transform leftParticipantsHolder;
    public InputField nameOfLocalParticipant;
    public Button joinButton;
    public Toggle useWebCamToggle;
    public Dropdown webCamListDropdown;
    public Toggle useMicToggle;
    public Dropdown micListDropdown;
    public Button hangUpButton;
    public CameraPreview cameraPreview;
    
    public GameObject btnsController;
    public GameObject deviceScreen;
    
    public Dropdown statsDropDown;
    public Dropdown statsFrameDropDown;
    public Dropdown statsTestDropDown;
    public GameObject loginScreen;
    public Slider withSlider;
    public Slider heightSlider;
    public Slider frameSlider;
    
    public TextMeshProUGUI withText;
    public TextMeshProUGUI heightText;
    public TextMeshProUGUI frameText;
    public TextMeshProUGUI statsText;
    
    public Image audiStateImage;
    public Image videoStateImage;


    public Sprite enabledAudio;
    public Sprite disabledAudio;
    public Sprite disabledVideo;
    public Sprite enabledVideo;
    
    public int currentFrameSliderValue;
    public int currentWithSliderValue;
    public int currentHeightSliderValue;
    public static int MaxWidth = 1280;
    public  static int MaxHeight = 720;
    
    
    public Dictionary<string, double> scaleResolutionDownOptions = new Dictionary<string, double>()
    {
        { "Not scaling"+(MaxWidth/1)+"x"+(MaxHeight/1), 1.0f },
        { (MaxWidth/2)+"x"+(MaxHeight/2), 2.0f },
        { (MaxWidth/4)+"x"+(MaxHeight/4), 4.0f },
        { (MaxWidth/6)+"x"+(MaxHeight/6), 6.0f },
        { (MaxWidth/8)+"x"+(MaxHeight/8), 8.0f },
        { (MaxWidth/16)+"x"+(MaxHeight/16), 16.0f }
    };  
    public Dictionary<string, ulong> frameRateDownOptions = new Dictionary<string, ulong>()
    {
        { "Framerate 60", 2000 },
        { "Framerate 55", 1500 },
        { "Framerate 50", 1000},
        { "Framerate 45", 850 },
        { "Framerate 40", 600 },
        { "Framerate 35", 500 },
        { "Framerate 30", 450 },
        { "Framerate 25", 400 },
        { "Framerate 20", 300 },
        { "Framerate 15", 250 },
        { "Framerate 10", 125 }
    };


    public GameObject chatObject;
    public ChatPanelExample chatPanelExample;

    public CanvasGroup chatCanvas;
     public void withSliderValueChanged(Slider slider)
    {
        switch (slider.value)
        {
            case 0:
                withText.text = "With:144";
               SessionManager.minWidth = 144;
                break;
            case 1:
                withText.text = "With:176";
                SessionManager.minWidth = 176;
                break;
            case 2:
                withText.text = "With:240";
                SessionManager.minWidth = 240;
                break;
            case 3:
                withText.text = "With:320";
                SessionManager.minWidth = 320;
                break;
            case 4:
                withText.text = "With:480";
                SessionManager.minWidth = 480;
                break;
            case 5:
                withText.text = "With:640";
                SessionManager.minWidth = 640;
                break;
            case 6:
                withText.text = "With:720";
                SessionManager.minWidth = 720;
                break;
            case 8:
                withText.text = "With:1024";
                SessionManager.minWidth = 1024;
                break;
            case 9:
                withText.text = "With:1280";
                SessionManager.minWidth = 1280;
                break;
        }
    }
    public void heightSliderValueChanged(Slider slider)
    {
        switch (slider.value)
        {
            case 0:
                heightText.text = "Height:144";
                SessionManager.minHeight = 144;
                break;
            case 1:
                heightText.text = "Height:176";
                SessionManager.minHeight = 176;
                break;
            case 2:
                heightText.text = "Height:240";
                SessionManager.minHeight = 240;
                break;
            case 3:
                heightText.text = "Height:320";
                SessionManager.minHeight = 320;
                break;
            case 4:
                heightText.text = "Height:480";
                SessionManager.minHeight = 480;
                break;
            case 5:
                heightText.text = "Height:640";
                SessionManager.minHeight = 640;
                break;
            case 6:
                heightText.text = "Height:720";
                SessionManager.minHeight = 720;
                break;
            case 8:
                heightText.text = "Height:1024";
                SessionManager.minHeight = 1024;
                break;
            case 9:
                heightText.text = "Height:1280";
                SessionManager.minHeight = 1280;
                break;
        }
    }
    public void framesSliderValueChanged(Slider slider)
    {
        switch (slider.value)
        {
            case 0:
                frameText.text = "Frames:10";
                SessionManager.instance.targetFrameRate = 10;
                break;
            case 1:
                frameText.text = "Frames:15";
                SessionManager.instance.targetFrameRate = 15;
                break;
            case 2:
                frameText.text = "Frames:20";
                SessionManager.instance.targetFrameRate = 20;
                break;
            case 3:
                frameText.text = "Frames:25";
                SessionManager.instance.targetFrameRate = 25;
                break;
            case 4:
                frameText.text = "Frames:30";
                SessionManager.instance.targetFrameRate = 30;
                break;
            case 5:
                frameText.text = "Frames:35";
                SessionManager.instance.targetFrameRate = 35;
                break;
            case 6:
                frameText.text = "Frames:40";
                SessionManager.instance.targetFrameRate = 40;
                break;
            case 7:
                frameText.text = "Frames:45";
                SessionManager.instance.targetFrameRate = 45;
                break;
            case 8:
                frameText.text = "Frames:50";
                SessionManager.instance.targetFrameRate = 50;
                break;
            case 9:
                frameText.text = "Frames:55";
                SessionManager.instance.targetFrameRate = 55;
                break;
            case 10:
                frameText.text = "Frames:60";
                SessionManager.instance.targetFrameRate = 60;
                break;
        
        }
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        Application.targetFrameRate = 100;
        Application.runInBackground = true;
        QualitySettings.vSyncCount = 0;
    
        useWebCamToggle.onValueChanged.AddListener(SwitchUseWebCam);
        
        hangUpButton.onClick.AddListener(Disconnect);
        useMicToggle.onValueChanged.AddListener(SwitchUseMic);
        statsDropDown.options = scaleResolutionDownOptions
            .Select(pair => new Dropdown.OptionData { text = pair.Key })
            .ToList();   
        statsFrameDropDown.options = frameRateDownOptions
            .Select(pair => new Dropdown.OptionData { text = pair.Key })
            .ToList();


        currentFrameSliderValue = 1;
        currentWithSliderValue = 3;
        currentHeightSliderValue = 2;
        yield return StartCoroutine(GetMicList());
        yield return StartCoroutine(GetWebCamsList());
    }

    private void OnEnable()
    {
        SessionManager.instance.onRemoteParticipantJoined += SetParticipantPlace;
        SessionManager.instance.onLocalParticipantJoined += SetParticipantPlace;
        SessionManager.instance.onConnected += UserConnected;
        SessionManager.instance.onDisconnected += UpdateUI;
        SessionManager.instance.onRemoteParticipantLeft += SetLeftParticipantPlace;
        SessionManager.instance.onLocalConnectionLost += ConnectionLost;
        SessionManager.instance.onLocalReconnecting += ReconnectingToSession;
        SessionManager.instance.onLocalConnectionResumed += ConnectionRestored;
        SessionManager.instance.onError += OnSessionError;

    }

    private void OnDisable()
    {
        SessionManager.instance.onRemoteParticipantJoined -= SetParticipantPlace;
        SessionManager.instance.onLocalParticipantJoined -= SetParticipantPlace;
        SessionManager.instance.onDisconnected -= UpdateUI;
        SessionManager.instance.onRemoteParticipantLeft -= SetLeftParticipantPlace;
        SessionManager.instance.onLocalConnectionLost -= ReconnectingToSession;
        SessionManager.instance.onLocalConnectionResumed -= ConnectionRestored;
    }

    public IEnumerator GetWebCamsList()
    {
        if (WebCamTexture.devices.Length == 0)
        {
            Debug.LogFormat("WebCam device not found");
            SwitchUseWebCam(false);
            yield break;
        }

        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            Debug.LogFormat("authorization for using the device is denied");
            SwitchUseWebCam(false);
            yield break;
        }
        webCamListDropdown.options = WebCamTexture.devices.Select(x => new Dropdown.OptionData(x.name)).ToList();
    }
    public IEnumerator GetMicList()
    {
        if (Microphone.devices.Length == 0)
        {
            Debug.LogFormat("micro not found");
            yield break;
        }

        yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);
        if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
        {
            Debug.LogFormat("authorization for using the micro is denied");
            yield break;
        }
        
        micListDropdown.options = Microphone.devices.Select(x => new Dropdown.OptionData(x)).ToList();
    }
    
    public void ShowDevices()
    {
        deviceScreen.SetActive(true);
    }

    public void ChangeCamIndex()
    {
       cameraPreview.changeCameraDevice(webCamListDropdown.value);
        Debug.Log("ID camera "+ cameraPreview.cameraID);
    }

    public void ConnectCall()
    {
        SessionManager.instance.Connect();
    }

    public void UserConnected(List<NewParticipant> participants)
    {
        
    }

    public void Disconnect()
    {
        SessionManager.instance.Disconnect();
        UpdateUI();
    }

    public void UpdateUI()
    {
     
        useWebCamToggle.interactable = true;
        webCamListDropdown.interactable = false;
        useMicToggle.interactable = true;
        micListDropdown.interactable = false;
        joinButton.interactable = true;
        hangUpButton.interactable = false;
        useWebCamToggle.isOn = false;
        useMicToggle.isOn = false;
        btnsController.SetActive(false);
        deviceScreen.SetActive(false);
        loginScreen.SetActive(true);
        chatCanvas.alpha = 0;
        connectionLostObject.SetActive(false);
        chatPanelExample.wcp.Clear();
    }
    
    public void HideUIButtons()
    {
        
        useWebCamToggle.interactable = false;
      
        useMicToggle.interactable = false;
      
        joinButton.interactable = false;
        hangUpButton.interactable = true;
        
        deviceScreen.SetActive(false);
        btnsController.SetActive(true);
       
    }
    
    public void MuteParticipantAudio()
    {
        Debug.Log("media "+mediaStateOptions.ToString());
        if (mediaStateOptionsAudio == MediaStateOptions.ENABLED)
            mediaStateOptionsAudio = MediaStateOptions.DISABLED;
        else
        {
            mediaStateOptionsAudio = MediaStateOptions.ENABLED;
        }
        // SessionManager.instance.MuteParticipantAudio();
       SessionManager.instance.onParticipantMediaStateChanged(SessionManager.instance.localParticipant.id,MediaTypes.AUDIO,mediaStateOptionsAudio);
        audiStateImage.sprite = mediaStateOptionsAudio == MediaStateOptions.DISABLED ? disabledAudio : enabledAudio;
    }

    public MediaStateOptions mediaStateOptions;
    public MediaStateOptions mediaStateOptionsAudio;
    public void MuteParticipantVideo()
    {
     
        if (mediaStateOptions == MediaStateOptions.ENABLED)
            mediaStateOptions = MediaStateOptions.DISABLED;
        else
        {
            mediaStateOptions = MediaStateOptions.ENABLED;
        }
        SessionManager.instance.onParticipantMediaStateChanged(SessionManager.instance.localParticipant.id,MediaTypes.VIDEO, mediaStateOptions);
        videoStateImage.sprite = mediaStateOptions == MediaStateOptions.ENABLED ? enabledVideo : disabledVideo;
    }
    
    public void SwitchUseWebCam(bool isOn)
    {
        webCamListDropdown.interactable = isOn;
        SessionManager.instance.isVideoEnabled = isOn;
        mediaStateOptions = SessionManager.instance.isVideoEnabled
            ? MediaStateOptions.ENABLED
            : MediaStateOptions.DISABLED;
        if (SessionManager.instance.isVideoEnabled)
        {
            Debug.LogWarning("is enabled "+SessionManager.instance.isVideoEnabled);
            StartCoroutine(cameraPreview.StartRecording());
        }
        else
        {
            cameraPreview.camTexture.Stop();
            cameraPreview.camPreviewObject.SetActive(false);
        }
    }

    private void SwitchUseMic(bool isOn)
    {
        micListDropdown.interactable = isOn;
        SessionManager.instance.isAudioEnabled = isOn;
        mediaStateOptionsAudio = SessionManager.instance.isAudioEnabled
            ? MediaStateOptions.ENABLED
            : MediaStateOptions.DISABLED;
    }
    public void CallSettings()
    {
        frameSlider.value = currentFrameSliderValue;
        withSlider.value = currentWithSliderValue;
        heightSlider.value = currentHeightSliderValue;
    }
    
    public void ApplySettings()
    { 
        SessionManager.instance.ApplySettingsForSession();
        currentFrameSliderValue = (int)frameSlider.value;
        currentWithSliderValue = (int)withSlider.value;
        currentHeightSliderValue = (int)heightSlider.value;
        
    }

    public void setMicrophoneDeviceIndex()
    {
        SessionManager.instance.microphoneDeviceID = micListDropdown.value;
    }

    public void ChangeUserName()
    {
        SessionManager.instance.ParticipantName = nameOfLocalParticipant.text;
    }

    public void CallConnectedState()
    {
        StartCoroutine(WaitConnectedToServerState());
    }

    IEnumerator WaitConnectedToServerState()
    {
        while (!SessionManager.instance.isConnected)
        {
            yield return null;
        }

        SessionManager.instance.onMessageReceived += RecieveMessageFromSocket;
    }

    public InputField chatInputField;
    public void SendMessageToChat()
    {
        SessionManager.instance.SendMessageInChat(chatInputField.text);
        chatPanelExample.wcp.AddChatAndUpdate(false,chatInputField.text,SessionManager.instance.localParticipant.ParticipantName);
       
        chatInputField.clear();
    }

    public void RecieveMessageFromSocket(string message,string participantName)
    {
        Debug.Log("we recieve "+message);
        chatPanelExample.wcp.AddChatAndUpdate(true,message,participantName);
    }

    public void SetParticipantPlace(Participant participant)
    {
        HideUIButtons();
        participant.gameObject.transform.SetParent(participantsHolder);

        participant.gameObject.transform.localScale = new Vector3(-1f, 1f, 1f);
        participant.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
    }

    public GameObject connectionLostObject;
    public Image connectionLostImage;
    public Text connectionLostText;

    public void OnSessionError(SessionError error)
    {
        Debug.Log(error.ToString());
    }

    public void ConnectionLost()
    {
        connectionLostImage.color = Color.red;
        connectionLostText.text = "Connection lost...";
        connectionLostObject.SetActive(true);
    }    
    public void ReconnectingToSession()
    {
        connectionLostImage.color = Color.yellow;
        connectionLostText.text = "Reconnecting...";
        connectionLostObject.SetActive(true);
    }

    public void ConnectionRestored()
    {
        StartCoroutine(IEConnectionRestored());
    }

    public IEnumerator IEConnectionRestored()
    {
      connectionLostImage.color = Color.green;
      connectionLostText.text = "Connection restored...";
        yield return  new WaitForSeconds(4f);
      
        connectionLostObject.SetActive(false);
        
    }

    public void SetLeftParticipantPlace(Participant participant)
    {
        //participant.gameObject.SetActive(false);
    }

    public void DestroyParticipant(GameObject participant)
    {
        Destroy(participant);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
public static class Extension
{
    public static void clear(this InputField inputfield)
    {
        inputfield.Select();
        inputfield.text = "";
    }
}